/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.allscore.batch.test.xtream;

import com.allscore.batch.test.xtream.body.InstgInf;
import com.allscore.batch.test.xtream.body.SgnInf;
import com.allscore.batch.test.xtream.body.TrxInf;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

/**
 *
 * @author Administrator
 */
@XStreamAlias("root")
public class Root {

    @XStreamAsAttribute
    private String xmlns = "namespace_string";

    @XStreamAlias("MsgHeader")
    private MsgHeader msgHeader;

    @XStreamAlias("MsgBody")
    private MsgBody msgBody;

    public MsgHeader getMsgHeader() {
        return msgHeader;
    }

    public void setMsgHeader(MsgHeader msgHeader) {
        this.msgHeader = msgHeader;
    }

    public MsgBody getMsgBody() {
        return msgBody;
    }

    public void setMsgBody(MsgBody msgBody) {
        this.msgBody = msgBody;
    }

    public String getXmlns() {
        return xmlns;
    }

    public void setXmlns(String xmlns) {
        this.xmlns = xmlns;
    }

    public static void main(String[] args) {
        // MsgHeader
        MsgHeader msgHeader = new MsgHeader();

        // MsgBody
        MsgBody msgBody = new MsgBody();
        SgnInf sgnInf = new SgnInf();
        msgBody.setSgnInf(sgnInf);
        TrxInf trxInf = new TrxInf();
        msgBody.setTrxInf(trxInf);
        InstgInf instgInf = new InstgInf();
        msgBody.setInstgInf(instgInf);

        // root
        Root root = new Root();
        root.setMsgBody(msgBody);
        root.setMsgHeader(msgHeader);

        XStream xStream = new XStream();

//        xStream.alias("root", Root.class);
        xStream.autodetectAnnotations(true);
        xStream.useAttributeFor(Root.class, "xmlns");
        String rootHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
        String xml = rootHeader + "\r\n" + xStream.toXML(root);
        System.out.println(xml);
    }

}
