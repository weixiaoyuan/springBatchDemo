package com.allscore.batch.test.handler;

import com.allscore.batch.test.entity.Product;
import com.allscore.batch.test.repository.ProductRepository;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 自定义输出,实现ItemWriter接口
 */
@Component
public class CsvJPAWriter implements ItemWriter<Product> {

    @Autowired
    private EntityManager em;

    @Autowired
    private ProductRepository productRepository;

    @Override
    public void write(List<? extends Product> items) throws Exception {
        try {
            productRepository.save(items);
        } catch (Exception e) {
            e.printStackTrace();
        }

//		for(Product simpleTable : items) {
//			//TODO *********************异步并发时，不得在processor writer抛出异常...手动捕获异常
//			try {
//				em.persist(simpleTable);//瞬时态实体(原本数据库不存在的) --> 持久化态实体(会增加到持久化实体管理容器，set就会更新数据库)
//				//persist需要等spring batch把这一批执行完了再发起em.flush()提交
//				//这里的perist执行完如果直接执行detach()；则不会提交到数据库，因为em.flush()还未执行
//				//但是在这里手动每条执行flush就跟下面的merge一样了，每条数据提交一次，还不如等一批数据完了，spring-batch会发起em.flush()
//				
////				em.merge(simpleTable);//瞬时态实体或游离态实体 --> 持久化态实体(实时提交到数据库归档，性能会不完美，因为每条提交一次，还要检查一次数据库是否已经存在)
////				em.detach(simpleTable);//将持久化态 转化为 游离态，并从持久化实体管理容器删除，后续set不会造成更新数据库
//			} catch (Exception e) {
//				e.printStackTrace();
//				//错误处理操作...
//			}
//    }

}

}
