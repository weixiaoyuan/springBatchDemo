/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.allscore.batch.test.xtream.body;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 *
 * @author Administrator
 */
public class SgnInf {

    @XStreamAlias("SgnAcctIssrId")
    private String sgnAcctIssrId = "";

    @XStreamAlias("SgnAcctTp")
    private String sgnAcctTp = "";

    @XStreamAlias("SgnAcctId")
    private String sgnAcctId = "";

    @XStreamAlias("SgnAcctNm")
    private String sgnAcctNm = "";

    @XStreamAlias("IDTp")
    private String iDTp = "";

    @XStreamAlias("IDNo")
    private String iDNo = "";

    @XStreamAlias("MobNo")
    private String mobNo = "";

    public String getSgnAcctIssrId() {
        return sgnAcctIssrId;
    }

    public void setSgnAcctIssrId(String sgnAcctIssrId) {
        this.sgnAcctIssrId = sgnAcctIssrId;
    }

    public String getSgnAcctTp() {
        return sgnAcctTp;
    }

    public void setSgnAcctTp(String sgnAcctTp) {
        this.sgnAcctTp = sgnAcctTp;
    }

    public String getSgnAcctId() {
        return sgnAcctId;
    }

    public void setSgnAcctId(String sgnAcctId) {
        this.sgnAcctId = sgnAcctId;
    }

    public String getSgnAcctNm() {
        return sgnAcctNm;
    }

    public void setSgnAcctNm(String sgnAcctNm) {
        this.sgnAcctNm = sgnAcctNm;
    }

    public String getiDTp() {
        return iDTp;
    }

    public void setiDTp(String iDTp) {
        this.iDTp = iDTp;
    }

    public String getiDNo() {
        return iDNo;
    }

    public void setiDNo(String iDNo) {
        this.iDNo = iDNo;
    }

    public String getMobNo() {
        return mobNo;
    }

    public void setMobNo(String mobNo) {
        this.mobNo = mobNo;
    }

}
