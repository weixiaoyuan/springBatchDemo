/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.allscore.batch.test.xtream;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.security.AnyTypePermission;

/**
 *
 * @author Administrator
 */
@XStreamAlias("MsgHeader")
public class MsgHeader {

//    @XStreamAlias("SndDt")
//    private String sndDt = "";
    @XStreamAlias("MsgTp")
    private String msgTp = "";

    @XStreamAlias("IssrId")
    private String issrId = "";

    @XStreamAlias("Drctn")
    private String drctn = "";

    @XStreamAlias("SignSN")
    private String signSN = "";

    @XStreamAlias("NcrptnSN")
    private String ncrptnSN = "";

    @XStreamAlias("DgtlEnvlp")
    private String dgtlEnvlp = "";

//    public String getSndDt() {
//        return sndDt;
//    }
//
//    public void setSndDt(String sndDt) {
//        this.sndDt = sndDt;
//    }
    public String getMsgTp() {
        return msgTp;
    }

    public void setMsgTp(String msgTp) {
        this.msgTp = msgTp;
    }

    public String getIssrId() {
        return issrId;
    }

    public void setIssrId(String issrId) {
        this.issrId = issrId;
    }

    public String getDrctn() {
        return drctn;
    }

    public void setDrctn(String drctn) {
        this.drctn = drctn;
    }

    public String getSignSN() {
        return signSN;
    }

    public void setSignSN(String signSN) {
        this.signSN = signSN;
    }

    public String getNcrptnSN() {
        return ncrptnSN;
    }

    public void setNcrptnSN(String ncrptnSN) {
        this.ncrptnSN = ncrptnSN;
    }

    public String getDgtlEnvlp() {
        return dgtlEnvlp;
    }

    public void setDgtlEnvlp(String dgtlEnvlp) {
        this.dgtlEnvlp = dgtlEnvlp;
    }

    public static void main(String[] args) {
//        MsgHeader msgHeader = new MsgHeader();
//        msgHeader.setSignSN("100");
//        msgHeader.setIssrId("22");
//        XStream xStream = new XStream();
//        xStream.autodetectAnnotations(true);
//        xStream.alias("MsgHeader", msgHeader.getClass());
//        System.out.println(xStream.toXML(msgHeader));

        String xml = "<MsgHeader>\n"
//                + "  <SndDt1></SndDt1>\n"
//                + "  <SndDt></SndDt>\n"
                + "  <MsgTp></MsgTp>\n"
                + "  <IssrId>22</IssrId>\n"
                + "  <Drctn></Drctn>\n"
                + "  <SignSN>100</SignSN>\n"
                + "  <NcrptnSN></NcrptnSN>\n"
                + "  <DgtlEnvlp></DgtlEnvlp>\n"
                + "</MsgHeader>";
        XStream xStream = new XStream(new DomDriver("UTF-8"));
        // 设置默认的安全校验
//        XStream.setupDefaultSecurity(xStream);
        // 使用本地的类加载器
//        xStream.setClassLoader(XStreamUtil.class.getClassLoader());
        // 允许所有的类进行转换
        xStream.addPermission(AnyTypePermission.ANY);
        xStream.processAnnotations(Root.class);
        xStream.ignoreUnknownElements();
        // Auto-detect Annotations 自动侦查注解 
        xStream.autodetectAnnotations(true);
        MsgHeader root = (MsgHeader) xStream.fromXML(xml);

        System.out.println(root.getIssrId());
    }

}
