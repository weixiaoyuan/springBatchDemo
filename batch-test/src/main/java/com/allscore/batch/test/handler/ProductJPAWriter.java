/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.allscore.batch.test.handler;

import com.alibaba.fastjson.JSONObject;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

/**
 *
 * @author Administrator
 */
@Component
public class ProductJPAWriter implements ItemWriter<JSONObject> {

    private Logger logger = LoggerFactory.getLogger(getClass());

//    @Autowired
//    private RabbitMQSendService rabbitMQSendService;
//
//    @Autowired
//    @Qualifier("mercNotifyRabbitTemplate")
//    private RabbitTemplate mercNotifyRabbitTemplate;

    @Override
    public void write(List<? extends JSONObject> items) throws Exception {

        items.forEach((object) -> {
            try {
                String quenaName = object.getString("queneName");
                // 推送到其他地方操作
                // rabbitMQSendService.sendToQueue(mercNotifyRabbitTemplate, quenaName, object);

            } catch (Exception e) {
                logger.error("发送到队列异常：", e);
            }
        });

    }
}
