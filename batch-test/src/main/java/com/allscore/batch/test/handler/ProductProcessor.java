/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.allscore.batch.test.handler;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.allscore.batch.test.entity.Product;
import javax.persistence.EntityManager;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 中间数据转换器，将输入的SimpleTable更新<br>
 * ItemProcessor<I，O>，I需要对应reader的实现， O需要对应writer的实现
 */
@Component
public class ProductProcessor implements ItemProcessor<Product, JSONObject> {

    @Autowired
    private EntityManager entityManager;

    @Override
    public JSONObject process(Product i) throws Exception {
        i.setDescription("吹个牛，再吹个象");
        entityManager.merge(i);
        return (JSONObject) JSON.toJSON(i);
    }

}
