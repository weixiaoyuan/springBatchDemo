/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.allscore.batch.test.xtream.body;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 *
 * @author Administrator
 */
public class TrxInf {

    @XStreamAlias("TrxCtgy")
    private String trxCtgy = "";

    @XStreamAlias("TrxId")
    private String trxId = "";

    @XStreamAlias("TrxDtTm")
    private String trxDtTm = "";

    @XStreamAlias("AuthMsg")
    private String authMsg = "";

    public String getTrxCtgy() {
        return trxCtgy;
    }

    public void setTrxCtgy(String trxCtgy) {
        this.trxCtgy = trxCtgy;
    }

    public String getTrxId() {
        return trxId;
    }

    public void setTrxId(String trxId) {
        this.trxId = trxId;
    }

    public String getTrxDtTm() {
        return trxDtTm;
    }

    public void setTrxDtTm(String trxDtTm) {
        this.trxDtTm = trxDtTm;
    }

    public String getAuthMsg() {
        return authMsg;
    }

    public void setAuthMsg(String authMsg) {
        this.authMsg = authMsg;
    }

}
