/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.allscore.batch.test.xtream.body;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 *
 * @author Administrator
 */
public class InstgInf {

    @XStreamAlias("InstgId")
    private String instgId;

    @XStreamAlias("InstgAcct")
    private String instgAcct;

    public String getInstgId() {
        return instgId;
    }

    public void setInstgId(String instgId) {
        this.instgId = instgId;
    }

    public String getInstgAcct() {
        return instgAcct;
    }

    public void setInstgAcct(String instgAcct) {
        this.instgAcct = instgAcct;
    }

    @Override
    public String toString() {
        return "InstgInf{" + "instgId=" + instgId + ", instgAcct=" + instgAcct + '}';
    }

}
