/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.allscore.batch.test.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import org.springframework.util.StringUtils;

/**
 *
 * @author Administrator
 */
public class DateUtils {

    public static String getNowDate() {
        return convertJavaDateToStr(null, "yyyyMMdd");
    }

    public static String convertJavaDateToStr(Date srcDate, String format) {
        if (format == null || "".equals(format.trim())) {
            format = "yyyyMMddHHmmss";
        }
        if (srcDate == null) {
            srcDate = new Date();
        }
        SimpleDateFormat toFormat = new SimpleDateFormat(format);
        return toFormat.format(srcDate);
    }

    public static String getNowTime() {
        return convertJavaDateToStr(null, "HHmmss");
    }

    public static String genUID_normal() {
        return UUID.randomUUID().toString().replace("-", "").toUpperCase();
    }

    public static Date getNowJavaDate() {
        return new Date();
    }

    public static String getNowDateTime(String format) {
        if (StringUtils.isEmpty(format)) {
            format = "yyyyMMddHHmmss";
        }
        return convertJavaDateToStr(null, format);
    }

    /**
     * 验证IPv4地址
     *
     * @param 待验证的字符串
     * @return 如果是符合格式的字符串,返回 true,否则为false
     */
    public static boolean isIPv4(String str) {
        if (StringUtils.isEmpty(str)) {
            return false;
        }
        String num = "(25[0-5]|2[0-4]\\d|[0-1]\\d{2}|[1-9]?\\d)";
        String regex = "^" + num + "\\." + num + "\\." + num + "\\." + num + "$";
        return str.matches(regex);
    }

    /**
     * 验证IPv6地址
     *
     * @param 待验证的字符串
     * @return 如果是符合格式的字符串,返回 true,否则为false
     */
    public static boolean isIPv6(String str) {
        if (StringUtils.isEmpty(str)) {
            return false;
        }
        boolean result = false;
        String regHex = "(\\p{XDigit}{1,4})";
        //没有双冒号  
        String regIPv6Full = "^(" + regHex + ":){7}" + regHex + "$";
        //双冒号在中间或者没有双冒号  
        String regIPv6AbWithColon = "^(" + regHex + "(:|::)){0,6}" + regHex + "$";
        //双冒号开头  
        String regIPv6AbStartWithDoubleColon = "^(" + "::(" + regHex + ":){0,5}" + regHex + ")$";

        String regIPv6 = "^(" + regIPv6Full + ")|(" + regIPv6AbStartWithDoubleColon + ")|(" + regIPv6AbWithColon + ")$";

        //下面还要处理地址为::的情形和地址包含多于一个的::的情况（非法）  
        if (str.indexOf(":") != -1) {
            if (str.length() <= 39) {
                String addressTemp = str;
                int doubleColon = 0;
                if (str.equals("::")) {
                    return true;
                }
                while (addressTemp.indexOf("::") != -1) {
                    addressTemp = addressTemp.substring(addressTemp.indexOf("::") + 2, addressTemp.length());
                    doubleColon++;
                }
                if (doubleColon <= 1) {
                    result = str.matches(regIPv6);
                }
            }
        }
        return result;
    }
}
