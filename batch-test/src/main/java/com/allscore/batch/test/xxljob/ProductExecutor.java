/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.allscore.batch.test.xxljob;

import com.alibaba.fastjson.JSONObject;
import com.allscore.batch.test.service.BatchService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHander;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Administrator
 */
@JobHander(value = "productBatch")
@Service
public class ProductExecutor extends IJobHandler {

    @Autowired
    private BatchService batchService;

    @Override
    public ReturnT<String> execute(String... strings) throws Exception {
        HashMap<String, Object> result = batchService.productBatch(3L);

        return new ReturnT<String>(ReturnT.SUCCESS_CODE, JSONObject.toJSONString(result));
    }

}
