package com.allscore.batch.test.handler;

import com.allscore.batch.test.entity.Product;
import com.allscore.batch.test.util.DateUtils;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;


/**
 * 中间数据转换器，将CVS转换成指定的内容输出<br>
 * ItemProcessor<I，O>，I需要对应reader的实现， O需要对应writer的实现
 */
@Component
public class CsvItemProcessor implements ItemProcessor<Product, Product>{

    @Override
    public Product process(Product i) throws Exception {
        return i;
    }

//	@Override
//	public Product process(Product item) throws Exception {
//		
////		//TODO *********************异步并发时，不得在processor writer抛出异常...手动捕获异常
////		try {
////			item.setName("123");
////			return item;
////		} catch (Exception e) {
////			e.printStackTrace();
////			return null;
////		}
//return 
//	}
	
}
