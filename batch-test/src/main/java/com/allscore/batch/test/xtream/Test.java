/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.allscore.batch.test.xtream;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

/**
 *  java bean对象与xml之间的转换，使用XStream
 * @author Administrator
 */
public class Test {

    public static void main(String[] args) {
        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
                + "<root xmlns=\"namespace_xml\">\n"
                + "  <MsgHeader>\n"
                + "    <SndDt>0</SndDt>\n"
                + "    <MsgTp>1</MsgTp>\n"
                + "    <IssrId>2</IssrId>\n"
                + "    <Drctn>3</Drctn>\n"
                + "    <SignSN>4</SignSN>\n"
                + "    <NcrptnSN>5</NcrptnSN>\n"
                + "    <DgtlEnvlp>6</DgtlEnvlp>\n"
                + "  </MsgHeader>\n"
                + "  <MsgBody>\n"
                + "    <SgnInf>\n"
                + "      <SgnAcctIssrId>7</SgnAcctIssrId>\n"
                + "      <SgnAcctTp>8</SgnAcctTp>\n"
                + "      <SgnAcctId>9</SgnAcctId>\n"
                + "      <SgnAcctNm>10</SgnAcctNm>\n"
                + "      <IDTp>11</IDTp>\n"
                + "      <IDNo>12</IDNo>\n"
                + "      <MobNo>13</MobNo>\n"
                + "    </SgnInf>\n"
                + "    <TrxInf>\n"
                + "      <TrxCtgy>14</TrxCtgy>\n"
                + "      <TrxId>15</TrxId>\n"
                + "      <TrxDtTm>16</TrxDtTm>\n"
                + "      <AuthMsg>17</AuthMsg>\n"
                + "    </TrxInf>\n"
//                + "    <InstgInf>\n"
//                + "      <InstgId>18</InstgId>\n"
//                + "      <InstgAcct>19</InstgAcct>\n"
//                + "    </InstgInf>\n"
                + "  </MsgBody>\n"
                + "</root>";
        XStream xStream = new XStream(new DomDriver("UTF-8"));
        // 设置默认的安全校验
//        XStream.setupDefaultSecurity(xStream);
        // 使用本地的类加载器
//        xStream.setClassLoader(XStreamUtil.class.getClassLoader());
        // 允许所有的类进行转换
//        xStream.addPermission(AnyTypePermission.ANY);
        xStream.processAnnotations(Root.class);
        // Auto-detect Annotations 自动侦查注解 
        xStream.autodetectAnnotations(true);
        xStream.ignoreUnknownElements();// 过滤 xml 没有的元素
        Root root = (Root) xStream.fromXML(xml);

        System.out.println(root.getMsgBody().getSgnInf().getMobNo());
    }

}
