/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.allscore.batch.test.config;

import com.alibaba.fastjson.JSONObject;
import com.allscore.batch.test.entity.Product;
import com.allscore.batch.test.handler.ProductJPAPageReader;
import com.allscore.batch.test.handler.ProductJPAWriter;
import com.allscore.batch.test.handler.ProductListener;
import com.allscore.batch.test.handler.ProductProcessor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;

/**
 *
 * @author Administrator
 */
@Configuration
@EnableBatchProcessing
public class ProductBatch {

    @Autowired
    private ProductJPAPageReader productJPAPageReader;

    @Autowired
    private ProductJPAWriter productJPAWriter;

    @Autowired
    private ProductListener productListener;

    @Autowired
    private ProductProcessor productProcessor;

    @Bean
    public Step paymentResultStep(StepBuilderFactory stepBuilderFactory) {
        String stepName = "step_productStep";
        return stepBuilderFactory.get(stepName).<Product, JSONObject>chunk(100)// 一次读取100条,如果reader是分页的，一页拉多少条要与这一致。
                .reader(productJPAPageReader)// 配置输入reader
                .processor(productProcessor)// 配置中间数据转换器
                .writer(productJPAWriter)// 配置输出writer
                .faultTolerant()// 高级设置↓↓↓
                // .retry(Exception.class)//哪个异常需要重试
                .noRetry(Exception.class)// 不需要重试的异常
                // .retryLimit(0)//重试次数
                .skip(Exception.class)// 跳过一切异常， 所有异常继承自Exception
                // .noSkip(RamboRunTimeException.class)//不跳过的异常
                // .skipLimit(1)//最大跳过次数
                .taskExecutor(new SimpleAsyncTaskExecutor())// 默认并发方式
                .throttleLimit(2)// 并发数，小于数据源可用的连接，默认4
                .build();
    }

    /**
     * 配置任务 这个方法名会默认为bean的name，不要重复了，重复了就没法在外面@Autowired获取bean
     * @param jobs
     * @param paymentResultStep
     * @return 
     */
    @Bean
    public Job paymentResultJob(JobBuilderFactory jobs, @Qualifier("productStep") Step paymentResultStep) {
        String jobName = "job_productJob";
        return jobs.get(jobName).incrementer(new RunIdIncrementer()).listener(productListener)// 配置监听器
                .flow(paymentResultStep)// 配置第一步骤
                // .next(step)//配置下个步骤
                .end().build();
    }
}
