/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.allscore.batch.test.xtream;

import com.allscore.batch.test.xtream.body.InstgInf;
import com.allscore.batch.test.xtream.body.SgnInf;
import com.allscore.batch.test.xtream.body.TrxInf;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 *
 * @author Administrator
 */
@XStreamAlias("MsgBody")
public class MsgBody {

    @XStreamAlias("SgnInf")
    private SgnInf sgnInf;

    @XStreamAlias("TrxInf")
    private TrxInf trxInf;

    @XStreamAlias("InstgInf")
    private InstgInf InstgInf;

    public SgnInf getSgnInf() {
        return sgnInf;
    }

    public void setSgnInf(SgnInf sgnInf) {
        this.sgnInf = sgnInf;
    }

    public TrxInf getTrxInf() {
        return trxInf;
    }

    public void setTrxInf(TrxInf trxInf) {
        this.trxInf = trxInf;
    }

    public InstgInf getInstgInf() {
        return InstgInf;
    }

    public void setInstgInf(InstgInf InstgInf) {
        this.InstgInf = InstgInf;
    }

    public static void main(String[] args) {
        MsgBody msgBody = new MsgBody();
        SgnInf sgnInf = new SgnInf();
        msgBody.setSgnInf(sgnInf);
        TrxInf trxInf = new TrxInf();
        msgBody.setTrxInf(trxInf);

        InstgInf instgInf = new InstgInf();
        msgBody.setInstgInf(instgInf);

        XStream xStream = new XStream();
        xStream.alias("MsgBody", msgBody.getClass());
        System.out.println(xStream.toXML(msgBody));

    }

}
