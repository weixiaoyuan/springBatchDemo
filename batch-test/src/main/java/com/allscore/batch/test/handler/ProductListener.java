/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.allscore.batch.test.handler;

import com.allscore.batch.test.util.DateUtils;
import java.util.Collection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.stereotype.Component;

/**
 *
 * @author Administrator
 */
@Component
public class ProductListener extends JobExecutionListenerSupport {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void afterJob(JobExecution jobExecution) {

        Collection<StepExecution> steps = jobExecution.getStepExecutions();// 获取步骤结果
        steps.stream().map((stepResult) -> {
            logger.info(stepResult.getStepName() + "  -------步骤数据统计 BEGIN ----------------------------------");
            return stepResult;
        }).map((stepResult) -> {
            logger.info("总计输入记录数：" + stepResult.getReadCount());
            return stepResult;
        }).map((stepResult) -> {
            logger.info("总计输出记录数：" + stepResult.getWriteCount());
            return stepResult;
        }).map((stepResult) -> {
            logger.info("开始执行时间：" + DateUtils.convertJavaDateToStr(stepResult.getStartTime(), "yyyy-MM-dd HH:mm:ss"));
            return stepResult;
        }).map((stepResult) -> {
            logger.info("执行结束时间：" + DateUtils.convertJavaDateToStr(stepResult.getEndTime(), "yyyy-MM-dd HH:mm:ss"));
            return stepResult;
        }).map((stepResult) -> {
            logger.info("总计跳过记录数：" + stepResult.getSkipCount());
            return stepResult;
        }).map((stepResult) -> {
            logger.info("被ItemProcessor跳过的记录数：" + stepResult.getFilterCount());
            return stepResult;
        }).map((stepResult) -> {
            logger.info("输入失败跳过的记录数：" + stepResult.getReadSkipCount());
            return stepResult;
        }).map((stepResult) -> {
            logger.info("输出失败跳过的记录数：" + stepResult.getWriteSkipCount());
            return stepResult;
        }).map((stepResult) -> {
            logger.info("执行过程中失败跳过的记录数：" + stepResult.getProcessSkipCount());// 一般不会有这个数据
            return stepResult;
        }).map((stepResult) -> {
            logger.info("事务成功提交次数，非记录数：" + stepResult.getCommitCount());
            return stepResult;
        }).map((stepResult) -> {
            logger.info("事务回滚记录数，非记录数：" + stepResult.getRollbackCount());
            return stepResult;
        }).map((stepResult) -> {
            logger.info("退出(异常退出或执行完成)步骤后的状态：" + stepResult.getExitStatus().getExitCode());// UNKNOWN COMPLETED NOOP
            return stepResult;
        }).map((stepResult) -> {
            // FAILED STOPPED
            logger.info("步骤最终状态" + stepResult.getStatus());// STARTING STARTED FAILED STOPPING STOPPED COMPLETED
            return stepResult;
        }).forEachOrdered((stepResult) -> {
            logger.info(stepResult.getStepName() + "  -------步骤数据统计 END -------------------------------------");
        });
    }

    @Override
    public void beforeJob(JobExecution jobExecution) {
        logger.info("批量处理:" + jobExecution.getExitStatus());
    }

}
